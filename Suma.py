#!/usr/bin/env python3
#
# Copyright 2017 Lawrence José González Delgado
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Este es un programa para sumar. tiene la posibilidad de usar el
el resultado de una (solo una) operación previa en la suma que se este
realizando en el momento.

Solo se pueden usar resultados de una operación previa hechas en la
misma sesión; no se guarda un historial de resultados de otras sesiones.
"""


def comp(var):
    """Comprueba si var es un número, si es ans o hay que corregir."""
    while is_float(var) is False:
        # Si var es ans pero no han habido operaciones anteriormente se
        # pedirá que se corrija, pero si se hizo una operación var será
        # igual al resultado de la operación anterior.
        if var.lower().strip() == 'ans' and ans is None:
            var = input(
                '\nNo se han realizado operaciones anteriormente.'
                '\nIntroduzca un número: '
                )
        elif var.lower().strip() == 'ans':
            var = ans
        # Se pedirá corrección si var no es ans.
        else:
            var = input('\nSolo puede introducir números reales: ')
    return var


def is_float(var):
    """Distingue si var es float u otro tipo y devuelve bool."""
    try:
        if var == float(var):
            return True
    except Exception:
        return False


ans = None
repetir = None

print('\nSuma de dos números')
while repetir not in (['no', 'n']):
    print('\nEscriba ANS para usar el resultado de una operación previa.')
    num1 = comp(input('\nIntroduzca un número: ').lower())
    num2 = comp(input('\nIntroduzca otro número: ').lower())
    res = float(num1) + float(num2)
    # Si res es un número no decimal entonces se se convertirá en no
    # decimal, sino se mostrara como decimal.
    if res.is_integer() is True:
        res = int(res)
    print('\n{} + {} = {}'.format(num1, num2, res))
    ans = res

    repetir = input('\n¿Quiere realizar otra operación?: ').lower()
    # Si se escribe un argumento invalido se pedirá su reintroducción.
    while repetir not in (['sí', 'si', 's', 'no', 'n']):
        repetir = input('\nLas opciones son "sí" y "no": ')
