#!/usr/bin/env python3
#
# Copyright 2017 Lawrence José González Delgado
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Cara o sello

Este es un juego de cara o sello, donde el usuario elige uno de los dos, la
computadora aleatóriamente hará una elección y si esta coincide con la del
usuario entonces este gana.
"""

import random
repetir = 'sí'

while repetir not in (['no', 'n']):
    moneda = input('\nElija un lado de la moneda ¿cara o sello?: ').lower()
    # Si el argumento es invalido se deberá reintroducir.
    while moneda not in ('cara', 'sello'):
        moneda = input('\nSolo puede elegir "cara" o "sello": ')
    print('\nHa elegido {}.'.format(moneda))

    repetir = input(
        '\nHa salido {}.'.format(random.choice(['cara', 'sello'])) +
        '\n\n¿Quiere volver a jugar?: '
    ).lower()

    while repetir not in (['si', 'sí', 's', 'no', 'n']):
        repetir = input('\nLas opciones son "sí" o "no": ')
