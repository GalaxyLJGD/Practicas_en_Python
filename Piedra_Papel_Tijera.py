#!/usr/bin/env python3
#
# Copyright 2017 Lawrence José González Delgado
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Este es un juego de piedra, papel y tijera donde dos jugadores pueden
jugar entre si, pueden elegir sus nombres, jugar contra la computadora,
incluso pueden haber partidas de computadora vs. computadora, también
hay un marcador de puntos que se muestra al final de cada partida.
"""

import getpass
import random


def is_auto(var):
    """Determina si var es la computadora y devuelve bool."""
    if var.lower() == 'auto':
        return True
    else:
        return False


def ppt(var):
    """El jugador elige entre piedra, papel, o tijera y devuelve var_2.

    Los jugadores tienen tres opciones a tomar, si uno de ellos es la
    computadora entonces la elección se tomará de forma aleatoria.
    """
    if is_auto(var) is True:
        var_2 = random.choice(['piedra', 'papel', 'tijera'])
        return var_2
    else:
        var_2 = getpass.getpass(
            '\nEscriba su selección, {}: '.format(var)
        ).lower()
        # Si el jugador escribe mal la elección la deberá reintroducir.
        while var_2 not in ('piedra', 'papel', 'tijera'):
            var_2 = getpass.getpass(
                '\nSolo puede escribir "piedra", "papel" o "tijera": '
            ).lower()
        return var_2


def player_comp(var):
    """Devuelve texto si el jugador es auto o la variable entrante"""
    if is_auto(var) is True:
        return 'La Computadora'
    else:
        return var


name = '\nIntroduzca el nombre del Jugador {}: '
player_ask = '\nIntroduzca el nombre del Jugador {}: '
ply_vs_ply = '\n¡{} vs. {}!'
punt_1 = 0
punt_2 = 0
win = '\n¡{} gano!'
winner = '\n¡{} ha ganado la partida!'

print(
    'Piedra, Papel o Tijera.'
    + '\n\nElijan los nombres de sus jugadores.'
    + '\nSi quiere competir contra la computadora introduzca "auto" en '
    + 'cualquiera de los jugadores. En caso de querer un juego de computadora '
    + 'vs. computadora escriba "auto" en el nombre de ambos jugadores.'
)

# Introducción de los nombres de los jugadores
while True:
    player_1 = input(name.format('1'))
    player_2 = input(name.format('2'))
    if player_1 == player_2 and (player_1 or player_2) != 'auto':
        print('\nAmbos jugadores no pueden tener el mismo nombre: ')
    else:
        break
# Muestra player_1 vs. player_2.
print('\n{} vs. {}'.format(
    player_comp(player_1),
    player_comp(player_2)
))

while True:
    # No muestra el mensaje si ambos jugadores son auto.
    if (is_auto(player_1) and is_auto(player_2)) is False:
        print(
            '\nElijan Piedra, Papel o Tijera.'
            + '\nNo se preocupen si no ven lo que escriben, es para evitar '
            + 'que el otro jugador lo vea.'
        )

    # Toma y muestra de la elección de los jugadores.
    elec_1 = ppt(player_1)
    elec_2 = ppt(player_2)
    print('\n{} ha elegido {} y {} ha elegido {}.'.format(
        player_comp(player_1), elec_1, player_comp(player_2), elec_2
    ))

    # Se determina y muestra quien fue el ganador de la ronda o si es empate.
    if elec_1 == elec_2:
        print('\nEmpate.')
    elif (
        elec_1 == 'piedra' and elec_2 == 'papel'
        or elec_1 == 'papel' and elec_2 == 'tijera'
        or elec_1 == 'tijera' and elec_2 == 'piedra'
    ):
        punt_2 += 1
        print(win.format(player_comp(player_1)))
    else:
        punt_1 += 1
        print(win.format(player_comp(player_2)))

    close = input('\n¿Desea volver a jugar?: ').lower()
    # Si el argumento es invalido se volverá a preguntar.
    while close not in ('sí', 'si', 's', 'no', 'n'):
            close = input('\nLas opciones son "sí" o "no": ').lower()
    if close in ('no', 'n'):
        break

# Al terminar la partida se determina el ganador o si ha sido un empate.
if punt_1 == punt_2:
    print('\n¡Empate!')
elif punt_1 > punt_2:
    print(winner.format(player_comp(player_1)))
else:
    print(winner.format(player_comp(player_2)))
