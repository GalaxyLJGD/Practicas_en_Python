#!/usr/bin/env python3
#
# Copyright 2017 Lawrence José González Delgado
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Calculadora para la terminal

Esta calculadora tiene la posibilidad de usar el resultado de una (solo
una) operación previa en la que se este realizando en el momento.

Solo se pueden usar resultados de una operación previa hechas en la
misma sesión; no se guarda un historial de resultados de otras
sesiones.
"""


def comp(var):
    """Comprueba si var es un número, si es ans o hay que corregir."""
    while True:
        # Se distingue si var es un número o no y se realizan varias
        # operaciones más en caso de que no lo sea.
        try:
            float(var)
            return float(var)
        except ValueError:
            # Si var es ans pero no han habido operaciones anteriormente se
            # pedirá que se corrija, pero si se hizo una operación se
            # devolvera el resultado de aquella.
            if var.lower().strip() == 'ans':
                if ans is None:
                    var = input(
                        '\nNo se han realizado operaciones anteriormente.'
                        '\nIntroduzca un número: '
                    )
                else:
                    return ans
            # Se pedirá corrección si var no es ans.
            else:
                var = input('\nSolo puede introducir números reales: ')


def int_float(var):
    """Determina si var puede ser un número entero o no."""
    return int(var) if var.is_integer() is True else var


def num_input(arg_1, arg_2):
    """Pide el número y muestra el texto correspondiente."""
    return comp(input('Introduzca el {}: '.format(num_txt[arg_1][arg_2])))


# Variables predefinidas.
ans = None
menu = (
    '\n####################'
    '\n# Menú de Opciones #'
    '\n# 0: Salir.        #'
    '\n# 1: Sumar.        #'
    '\n# 2: Restar.       #'
    '\n# 3: Multiplicar.  #'
    '\n# 4: Dividir.      #'
    '\n####################'
    '\n'
    '\nIntroduzca el número de la operación que desea realizar: '
)
num_txt = {
    '1': ['primer sumando', 'segundo sumando', '+'],
    '2': ['minuendo', 'sustraendo', '-'],
    '3': ['multiplicando', 'multiplicador', '*'],
    '4': ['dividendo', 'multiplicador', '/'],
}

# Ciclo principal, el programa se cierra cuando el usuario elige la
# opción 0.
while True:
    opc = input('{}'.format(menu)).lower().strip()
    # Si el usuario introduce un argumento invalido cuando se le pedirá
    # que vuelva a reintroducirlo.
    while opc not in (['0', '1', '2', '3', '4']):
        opc = input('\nEl argumento es invalido.\n{}'.format(menu))

    if opc == '0':
        break

    print('\nEscriba ANS para usar el resultado de una operación previa.')

    # Se mostrará el texto y signo correspondiente a la operación
    # dependiendo de la elección del usuario.
    num_1 = num_input(opc, 0)
    num_2 = num_input(opc, 1)

    # Evita que se realice una división o resta entre 0.
    while opc in (['2', '4']) and num_2 == 0:
        # Se determina que texto mostrar dependiendo de la operación
        # elegida por el usuario.
        num_2 = float(comp(input(
            '\nNo puede realizar {} entre 0: '
            .format('restas' if opc == '2' else 'divisiones')
        )))

    # Realiza la operación matemática elegida por el usuario.
    if opc == '1':
        res = float(num_1) + float(num_2)
    elif opc == '2':
        res = float(num_1) - float(num_2)
    elif opc == '3':
        res = float(num_1) * float(num_2)
    elif opc == '4':
        res = float(num_1) / float(num_2)
    # Se muestra el resultado de la operación, ej: 3 - 4 = -1.
    print('\n{} {} {} = {}'.format(
        int_float(num_1), num_txt[opc][2], int_float(num_2), int_float(res)
    ))
    ans = res
