# Repositorio de Practicas en Python.

Este es un repositorio donde subiré mis practicas de programación en
**Python**. Intento que todo el código siga las guías de estilo de programación
del [PEP8](https://www.python.org/dev/peps/pep-0008/) y
[Hacking](https://docs.openstack.org/hacking/latest/).

Todos los archivos dentro de este repositorio están licenciados bajo la
[Licencia Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0).

---

This is a repository where I'll upload my **Python** programming practices. I
try to get all my code to follow
[PEP8](https://www.python.org/dev/peps/pep-0008/) and
[Hacking](https://docs.openstack.org/hacking/latest/) coding style guides.

All files inside this reporitory are licenser under
[Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0).

